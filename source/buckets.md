---
title: Buckets
---

Learn how to create, read, update and delete buckets.

<h2 id="create-bucket">Create Bucket</h2>

Create a new bucket

```js
POST /buckets
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.id`               | string | Resource id                | `"my-bucket"` |
| `data.type`             | string | Resource type              | `"buckets"`   |
| `data.attributes.title` | string | Title for this bucket      | `"My bucket"` |

<strong>Curl Example</strong>

```bash
$ curl -n -X POST https://api.twixly.com/v1/buckets \
  -d {
    "data": {
      "type": "buckets",
      "id": "my-bucket",
      "attributes": {
        "title": "My bucket",
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "buckets",
    "id": "my-bucket",
    "attributes": {
      "title": "My bucket",
      "description": ""
    },
    "meta": {
      "created_at": "2017-01-12T01:26:16.138Z",
      "modified_at": "2017-01-12T01:26:16.138Z",
      "created_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      }
    }
  }
}
```

<h2 id="get-bucket">Get Bucket</h2>

Get a bucket

```js
GET /buckets/<bucket_id>
```

<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id] \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "buckets",
    "id": "my-bucket",
    "attributes": {
      "title": "My bucket",
      "description": ""
    },
    "meta": {
      "created_at": "2017-01-12T01:26:16.138Z",
      "modified_at": "2017-01-12T01:26:16.138Z",
      "created_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      }
    }
  }
}
```

<h2 id="bucket-update">Update Bucket</h2>

Update a bucket

```js
PUT /buckets/<bucket_id>
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.id`               | string | Resource id                | `"my-bucket"` |
| `data.type`             | string | Resource type              | `"buckets"`   |
| `data.attributes.title` | string | Title for this bucket      | `"My bucket"` |

<strong>Curl Example</strong>

```bash
$ curl -n -X PUT https://api.twixly.com/v1/buckets/[bucket_id] \
  -d {
    "data": {
      "type": "buckets",
      "id": "my-bucket",
      "attributes": {
        "title": "My bucket",
        "description": "This description is updated"
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "buckets",
    "id": "my-bucket",
    "attributes": {
      "title": "My bucket",
      "description": "This description is updated"
    },
    "meta": {
      "created_at": "2017-01-12T01:26:16.138Z",
      "modified_at": "2017-01-12T01:26:16.138Z",
      "created_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      }
    }
  }
}
```

<h2 id="bucket-delete">Delete Bucket</h2>

Delete a bucket

```js
DELETE /buckets/<bucket_id>
```

<strong>Curl Example</strong>

```bash
$ curl -n -X DELETE https://api.twixly.com/v1/buckets/[bucket_id] \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "buckets",
    "id": "my-bucket",
    "attributes": {
      "title": "My bucket",
      "description": "This description is updated"
    },
    "meta": {
      "created_at": "2017-01-12T01:26:16.138Z",
      "modified_at": "2017-01-12T01:26:16.138Z",
      "created_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "55a594e43e60a7524d00001c"
        }
      }
    }
  }
}
```