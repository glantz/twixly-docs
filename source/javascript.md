---
title: JavaScript
---

To get started with the JavaScript SDK you can either install it using npm or render it as a standalone script in the browser.

<h2 id="javascript-installation">Installation</h2>

In node, using npm:

```js
npm install twixly
```

Download the standalone file from unpkg:

```xml
<script src="https://unpkg.com/twixly@latest/dist/twixly.min.js"></script>
```

<h2 id="javascript-first-request">Get your first content</h2>

The following code snippet is the most basic one you can use to get some content from Twixly with this SDK:

```js
var twixly = require('twixly')
var client = twixly.createClient({
  // This is the bucket ID.
  bucket: 'demo',
  // This is the access token for this bucket.
  accessToken: '5876db3831ad7e0a60b66ffd'
})

// This API call will request an item from the bucket defined in this client.
client.getItem('5876db3831ad7e0a60b66fad')
  .then((item) => console.log(item))
```

To see how it all works we've built a simple demo. Check it out at [Codepen](http://codepen.io/testcode/pen/LxOLGm?editors=1000). 