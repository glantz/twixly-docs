---
title: Extensions
---

Learn how to create, read, update and delete extensions.

<h2 id="create-extension">Create extension</h2>

Create a new extension

```js
POST /v1/buckets/<bucket_id>/extensions
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.type`             | string | Resource type | `"extensions"`   |
| `data.id` | string | Unique id   | `my-slug` |
| `data.attributes.title` | string |  Title for this extension | `"My slug"` |
| `data.attributes.uri` | string | URI to the extension   | `"https://example.com/slug"` |
| `data.attributes.type` | string |  Extension type. Can be "field" or "view" | `"field"` |
| `data.attributes.field_types` | array | Array of field types this extension supports    | `["string", "text", "number", "integer", "object", "array"]` |

<strong>Curl Example</strong>

```bash
$ curl -n -X POST https://api.twixly.com/v1/buckets/[bucket_id]/extensions \
  -d {
    "data": {
      "type": "extensions",
      "id": "my-slug",
      "attributes": {
        "title": "My slug",
        "uri": "https://example.com/slug",
        "type": "field",
        "field_types": "My slug",
        "description": "A short description"
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "extensions",
    "id": "my-slug",
      "attributes": {
        "title": "My slug",
        "uri": "https://example.com/slug",
        "type": "field",
        "field_types": "My slug",
        "description": "A short description"
      },
      "meta": {
        "bucket": {
          "data": {
            "type": "buckets",
            "id": "my-bucket"
          }
        },
        "status": "published",
        "position": 0,
        "created_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "modified_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "created_at": "2017-01-25T01:31:46.755Z",
        "modified_at": "2017-01-25T01:31:46.755Z"
      }
    }
  }
}
```

<h2 id="extension-type-get">Get extension</h2>

Get an extension

```js
GET /v1/buckets/<bucket_id>/extensions/<extension_id>
```

<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id]/extensions/[extension_id] \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "extensions",
    "id": "my-slug",
      "attributes": {
        "title": "My slug",
        "uri": "https://example.com/slug",
        "type": "field",
        "field_types": "My slug",
        "description": "A short description"
      },
      "meta": {
        "bucket": {
          "data": {
            "type": "buckets",
            "id": "my-bucket"
          }
        },
        "status": "published",
        "position": 0,
        "created_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "modified_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "created_at": "2017-01-25T01:31:46.755Z",
        "modified_at": "2017-01-25T01:31:46.755Z"
      }
    }
  }
}
```

<h2 id="extension-type-update">Update extension</h2>

Update an extension

```js
PUT /v1/buckets/<bucket_id>/extensions/<extension_id>
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.type`             | string | Resource type | `"extensions"`   |
| `data.id` | string | Unique id   | `my-slug` |
| `data.attributes.title` | string |  Title for this extension | `"My slug"` |
| `data.attributes.uri` | string | URI to the extension   | `"https://example.com/slug"` |
| `data.attributes.type` | string |  Extension type. Can be "field" or "view" | `"field"` |
| `data.attributes.field_types` | array | Array of field types this extension supports    | `["string", "text", "number", "integer", "object", "array"]` |

<strong>Curl Example</strong>

```bash
$ curl -n -X PUT https://api.twixly.com/v1/buckets/[bucket_id]/extensions/[extension_id] \
  -d {
    "data": {
      "type": "extensions",
      "id": "my-slug",
      "attributes": {
        "title": "My slug",
        "uri": "https://example.com/slug",
        "type": "field",
        "field_types": "My slug",
        "description": "A short description"
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "extensions",
    "id": "my-slug",
      "attributes": {
        "title": "My slug",
        "uri": "https://example.com/slug",
        "type": "field",
        "field_types": "My slug",
        "description": "A short description"
      },
      "meta": {
        "bucket": {
          "data": {
            "type": "buckets",
            "id": "my-bucket"
          }
        },
        "status": "published",
        "position": 0,
        "created_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "modified_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "created_at": "2017-01-25T01:31:46.755Z",
        "modified_at": "2017-01-25T01:31:46.755Z"
      }
    }
  }
}
```

<h2 id="extension-type-delete">Delete extension</h2>

Delete an extension

```js
DELETE /v1/buckets/<bucket_id>/extensions/<extension_id>
```

<strong>Curl Example</strong>

```bash
$ curl -n -X DELETE https://api.twixly.com/v1/buckets/[bucket_id]/extensions/[extension_id] \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "extensions",
    "id": "my-slug",
      "attributes": {
        "title": "My slug",
        "uri": "https://example.com/slug",
        "type": "field",
        "field_types": "My slug",
        "description": "A short description"
      },
      "meta": {
        "bucket": {
          "data": {
            "type": "buckets",
            "id": "my-bucket"
          }
        },
        "status": "published",
        "position": 0,
        "created_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "modified_by": {
          "data": {
            "type": "users",
            "id": "587d6426eeaf110800880087"
          }
        },
        "created_at": "2017-01-25T01:31:46.755Z",
        "modified_at": "2017-01-25T01:31:46.755Z"
      }
    }
  }
}
```