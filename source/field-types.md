---
title: Field Types
---

<!--
<h2 id="fields-string">Fields</h2>
-->

<p class="lead">
Twixly supports the following field types. The validation and schema is based on the [JSON Schema Specification](http://json-schema.org/).
</p>

<div style="margin-bottom: -30px;"></div>
## String

A short text field for titles, names, etc.
<div style="margin-bottom: -30px;"></div>
## Text

A long text field for paragraphs of text.
<div style="margin-bottom: -30px;"></div>
## Number

For storing decimal numbers, such as prices, quantities etc.
<div style="margin-bottom: -30px;"></div>
## Integer

For storing integer numbers.
<div style="margin-bottom: -30px;"></div>
## Boolean

For storing values that have two states, e.g. Yes or no, true or false etc.
<div style="margin-bottom: -30px;"></div>
## Object

For storing a simple object you have defined.
<div style="margin-bottom: -30px;"></div>
## Array

For storing a simple array of objects you have defined.
<div style="margin-bottom: -30px;"></div>
## Relationship

To model relationships between content, including other **Item Type**s. For example, linking a blog to a category.
<div style="margin-bottom: -30px;"></div>
## Media

For referencing media files such as images, videos, and documents.
<div style="margin-bottom: -30px;"></div>
## Location

Coordinate values for storing the latitude and longitude of a physical location.
<div style="margin-bottom: -30px;"></div>
## Field Type Formats

### Date and time

A timestamp value for storing date and time values. This is not a type but a format of the string type. Formats available for dates is "date" and "date-time".
