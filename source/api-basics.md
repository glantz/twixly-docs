---
title: Introduction
---

<div class="h2" style="margin-top: 25px;">Introduction</div>

Twixly is an API first headless content management system, offering a REST API for working with your content.
The API is available at [api.twixly.com](https://api.twixly.io) and it is based on the [JSON API Specification](http://jsonapi.org).


Building our API is an ongoing effort, and we rely on your feedback for what to focus on next. If you don't see a resource you want here, please let us know!

<h2 id="api-basics">API basics</h2>

The API uses standard HTTP verbs like GET, POST, PUT and DELETE. We use curl to illustrate examples here, but you can use any standard HTTP client to talk to the API.

<strong>All IDs are strings</strong>
All unique identifiers in the API are case sensitive strings and consist of alphanumeric characters.

<strong>All timestamps are UTC</strong>
Timestamps returned by the API are in the UTC timezone and in ISO8601 format.

<strong>All requests must be over HTTPS</strong>
Requests to the API must be made using HTTPS. Please remember to have your client validate SSL certificates.

<strong>All data must be encoded as UTF-8</strong>
Please encode all data using UTF-8. We will always return UTF-8 encoded responses.

<strong>All data must be valid JSON</strong>
We expect all data to be valid JSON, and will return HTTP error 400 Bad Request otherwise.

<strong>Always set the required headers</strong>
As we only support JSON data, the API expects the Content-Type header to be set to application/json.


<h2 id="errors">Errors</h2>

The API uses HTTP response codes to indicate success or failure of a request. Codes in the 2xx range indicate success, codes in 4xx range indicate validation errors or problems with the parameters you have provided (e.g missing required parameters) and codes in the 5xx range indicate errors on our side of things.

Here's what a usual error response looks like:

```js
{
  "errors": [
    {
      "status": 422,
      "source": { "pointer": "/data/attributes/title"},
      "title": "Invalid attribute",
      "detail": "First name must contain at least three characters."
    }
  ]
}
```

```js
{
  "errors": [
    {
      "status": 422,
      "source": { source: { parameter: "include"},
      "title": "Invalid query parameter",
      "detail": "The resource does not have an 'author' relationship path"
    }
  ]
}
```

See full specification at [JSON API Specification - Errors](http://jsonapi.org/examples/#error-objects-error-codes).