---
title: Introduction&#8291;
---

<div style="isplay:none;" class="subtitle-page">Headless Content Management. Build beautiful apps and web sites.</div>

<p class="lead" style="isplay:none;margin: -30px 0 -25px 0;">
We at Twixly want to make it fun for developers and editors to work with different types of content. For developers, we have tried to find common denominators that can help them achieve this and to end users, we have tried to create an intuitive user interface. To make it easy to get started, we've placed everything you need to know about Twixly in one place.
</p>

## Concepts

Twixlys Content Model is based on a pair of endpoints that are very powerful in what they can do.
They are Bucket, Item Type, Item and Media.

[Learn about our content model](/content-model.html)

## API Reference

Twixly is an API first headless content management system, offering a REST API for working with your content.
The API is available at api.twixly.com and it is based on the JSON API Specification.

[Check out the API docs](/api-basics.html)

## Extensions

Twixly Extensions lets you extend Twixly in a powerful way. You can create any type of field or view.
It’s your imagination that stops you.

[Extend Twixly with our creative extensions](/extensions-intro.html)

<h2>Platforms</h2>

[Check out the SDK for JavaScript](/javascript.html)
