---
title: Fetching data
---

<div class="h2" style="margin-top: 25px;">Introduction</div>

Data, including resources and relationships, can be fetched by sending a GET request to an endpoint. Responses can be further refined with the optional features described below.

<h2 id="fetching-data-item-type">Item type</h2>

Use the parameter item_type=[item_type_id]

```js
GET /v1/buckets/<bucket_id>/items?item_type=blog-post
```

<h2 id="fetching-data-get-item-type">Query items</h2>

Use these filter keys with any field:
`eq, gt, gte, lt, lte, ne, in, nin, exists`

* field[in]=20,40          Get all items where the field value is equal 20 or 40
* field[nin]=20,40         Get all items where the field value is not equal 20 or 40
* field[gt]=20             Get all items where the field value is greater than 20
* field[gte]=20            Get all items where the field value is greater than or equal to 20
* field[lt]=20             Get all items where the field value is lesser than 20
* field[lte]=20            Get all items where the field value is lesser than or equal to 20
* field[ne]=20,40          Get all items where the field value is not equal 20 or 40
* field[eq]=20,40          Get all items where the field value is equal 20 or 40

<strong>Example</strong>
```js
GET /v1/buckets/<bucket_id>/items?attributes.slug[eq]=my-first-post
```

<h2 id="fetching-data-select-fields">Select fields</h2>

The item_type query parameter must be included.

```js
GET /v1/buckets/<bucket_id>/items?select=id,attributes.title&item_type=blog-post
```

<h2 id="fetching-data-limit-fields">Limit fields</h2>

Default limit is 100 and the maximum limit is 1000.

```js
GET /v1/buckets/<bucket_id>/items?limit=20
```

<h2 id="fetching-data-sort">Sort</h2>

The sort order for each sort field is ascending unless it is prefixed with a minus ("-"), in which case it will be descending.

```js
GET /v1/buckets/<bucket_id>/items?sort=-attributes.title
```

<h2 id="fetching-data-paging">Paging</h2>

You can choose to limit the number of resources returned in a response to a subset ("page") of the whole set available. Twixly use an offset-based strategy with the parameters page[offset] and page[limit]

```js
GET /v1/buckets/<bucket_id>/items?page[offset]=0&page[limit]=2
```
<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id]/items?page[offset]=0&page[limit]=2 \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "meta": {
    "pagination": {
      "total_count": 41
    }
  },
  "links": {
    "self": "/v1/buckets/58ce8db6bef5cdfa4bc09e26/items?page[offset]=0&page[limit]=2",
    "first": "/v1/buckets/58ce8db6bef5cdfa4bc09e26/items?page[offset]=0&page[limit]=2",
    "next": "/v1/buckets/58ce8db6bef5cdfa4bc09e26/items?page[offset]=2&page[limit]=2",
    "last": "/v1/buckets/58ce8db6bef5cdfa4bc09e26/items?page[offset]=21&page[limit]=2"
  },
  "data": [
    {
      "type": "items",
      "id": "58d90303f28cbbf606bf9873",
    }
    },
    {
      "type": "items",
      "id": "58d15cd56a824509072c368a",
    }
  ]
}
```

<h2 id="fetching-data-relationships">Include Related Resources</h2>

Twixly support an include request parameter to allow the client to customize which related resources should be returned.

The value of the include parameter MUST be a comma-separated (",") list of relationship paths. A relationship path is a dot-separated ".") list of relationship names.

If Twixly is unable to identify a relationship path of resources from a path, it will respond with 400 Bad Request.

```js
GET /v1/buckets/<bucket_id>/items?include=page
```
<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id]/items?include=page \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": [
    {
      "type": "items",
      "id": "56ba709d9cd1962f7b30f026",
      "attributes": {
        "title": "Portfolio",
        "slug": "portfolio",
        "path": "/portfolio"
      },
      "relationships": {
        "page": {
          "data": {
            "type": "items",
            "id": "portfolio"
          }
        }
      }
    }
  },
  "included": [
    {
      "type": "items",
      "id": "portfolio",
      "attributes": {
        "title": "Portfolio",
        "description": "A description",
        "slug": "portfolio"
      },
    }
  }
}
```

See full specification at [JSON API Specification - Fetching data](http://jsonapi.org/format/#fetching).