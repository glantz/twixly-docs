---
title: Item Types
---

Learn how to create, read, update and delete item types.

<h2 id="create-item-type">Create Item Type</h2>

Create a new item type

```js
POST /item-types
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.id`               | string | Resource id                | `"post"` |
| `data.type`             | string | Resource type              | `"item-types"`   |
| `data.attributes.title` | string | Title for this item type      | `"Post"` |

<strong>Curl Example</strong>

```bash
$ curl -n -X POST https://api.twixly.com/v1/buckets/[bucket_id]/item-types \
  -d {
    "data": {
      "type": "item-types",
      "id": "post",
      "attributes": {
        "title": "Post",
        "description": "A simple blog post",
        "display_field": "title",
        "schema": {
          "type": "object",
          "properties": {
            "title": {
              "type": "string",
              "title": "Title"
            },
            "excerpt": {
              "type": "text",
              "title": "Excerpt"
            },
            "body": {
              "type": "text",
              "title": "Body",
              "format": "markdown"
            }
          },
          "required": [
            "title"
          ]
        }
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "item-types",
    "id": "post",
    "attributes": {
      "title": "Post",
      "description": "A simple blog post",
      "display_field": "title",
      "schema": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string",
            "title": "Title"
          },
          "excerpt": {
            "type": "text",
            "title": "Excerpt"
          },
          "body": {
            "type": "text",
            "title": "Body",
            "format": "markdown"
          }
        },
        "required": [
          "title"
        ]
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_at": "2017-01-25T01:06:32.021Z",
      "modified_at": "2017-01-25T01:06:32.021Z"
    }
  }
}
```

<h2 id="item-type-get">Get Item Type</h2>

Get a item type

```js
GET /item-types/<item_type_id>
```

<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id]/item-types/[item_type_id] \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "item-types",
    "id": "post",
    "attributes": {
      "title": "Post",
      "description": "A simple blog post",
      "display_field": "title",
      "schema": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string",
            "title": "Title"
          },
          "excerpt": {
            "type": "text",
            "title": "Excerpt"
          },
          "body": {
            "type": "text",
            "title": "Body",
            "format": "markdown"
          }
        },
        "required": [
          "title"
        ]
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_at": "2017-01-25T01:06:32.021Z",
      "modified_at": "2017-01-25T01:06:32.021Z"
    }
  }
}
```

<h2 id="item-type-update">Update Item Type</h2>

Update an item type

```js
PUT /item-types/<item_type_id>
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.id`               | string | Resource id                | `"post"` |
| `data.type`             | string | Resource type              | `"item-types"`   |
| `data.attributes.title` | string | Title for this item types      | `"Post"` |

<strong>Curl Example</strong>

```bash
$ curl -n -X PUT https://api.twixly.com/v1/buckets/[bucket_id]/item-types/[item_type_id] \
  -d {
    "data": {
      "type": "item-types",
      "id": "post",
      "attributes": {
        "title": "Post",
        "description": "A simple blog post",
        "display_field": "title",
        "schema": {
          "type": "object",
          "properties": {
            "title": {
              "type": "string",
              "title": "Title"
            },
            "excerpt": {
              "type": "text",
              "title": "Excerpt"
            },
            "body": {
              "type": "text",
              "title": "Body",
              "format": "markdown"
            }
          },
          "required": [
            "title"
          ]
        }
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "item-types",
    "id": "post",
    "attributes": {
      "title": "Post",
      "description": "A simple blog post",
      "display_field": "title",
      "schema": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string",
            "title": "Title"
          },
          "excerpt": {
            "type": "text",
            "title": "Excerpt"
          },
          "body": {
            "type": "text",
            "title": "Body",
            "format": "markdown"
          }
        },
        "required": [
          "title"
        ]
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_at": "2017-01-25T01:06:32.021Z",
      "modified_at": "2017-01-25T01:06:32.021Z"
    }
  }
}
```

<h2 id="item-type-delete">Delete Item Type</h2>

Delete an item type

```js
DELETE /item-types/<item_type_id>
```

<strong>Curl Example</strong>

```bash
$ curl -n -X DELETE https://api.twixly.com/v1/buckets/[bucket_id]/items-types/[item_type_id] \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "item-types",
    "id": "post",
    "attributes": {
      "title": "Post",
      "description": "A simple blog post",
      "display_field": "title",
      "schema": {
        "type": "object",
        "properties": {
          "title": {
            "type": "string",
            "title": "Title"
          },
          "excerpt": {
            "type": "text",
            "title": "Excerpt"
          },
          "body": {
            "type": "text",
            "title": "Body",
            "format": "markdown"
          }
        },
        "required": [
          "title"
        ]
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_at": "2017-01-25T01:06:32.021Z",
      "modified_at": "2017-01-25T01:06:32.021Z"
    }
  }
}
```