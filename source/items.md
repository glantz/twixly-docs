---
title: Items
---

Learn how to create, read, update and delete items.

<h2 id="create-item">Create Item</h2>

Create a new item

```js
POST /item
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.type`             | string | Resource type | `"items"`   |
| `data.attributes` | object | The JSON data associated to the item    | `{"title":"This is a item!"}` |

<strong>Curl Example</strong>

```bash
$ curl -n -X POST https://api.twixly.com/v1/buckets/[bucket_id]/items \
  -d {
    "data": {
      "type": "items",
      "attributes": {
        "title": "My first blog post",
        "excerpt": "A short excerpt",
        "body": "My body text"
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "items",
    "id": "5888000250dd41f022229fdc",
    "attributes": {
      "title": "My first blog post",
      "excerpt": "A short excerpt",
      "body": "My body text"
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "item_type": {
        "data": {
          "type": "item-types",
          "id": "post"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "created_at": "2017-01-25T01:31:46.755Z",
      "modified_at": "2017-01-25T01:31:46.755Z"
    }
  }
}
```

<h2 id="item-type-get">Get Item</h2>

Get an item

```js
GET /items/<item_id>
```

<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id]/items/[item_id] \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "items",
    "id": "5888000250dd41f022229fdc",
    "attributes": {
      "title": "My first blog post",
      "excerpt": "A short excerpt",
      "body": "My body text"
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "item_type": {
        "data": {
          "type": "item-types",
          "id": "post"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "created_at": "2017-01-25T01:31:46.755Z",
      "modified_at": "2017-01-25T01:31:46.755Z"
    }
  }
}
```

<h2 id="item-type-update">Update Item</h2>

Update an item

```js
PUT /items/<item_id>
```

<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.type`             | string | Resource type | `"5888000250dd41f022229fdc"`   |
| `data.id`             | string | Resource id | `"items"`   |
| `data.attributes` | object | The JSON data associated to the item    | `{"title":"This is a item!"}` |

<strong>Curl Example</strong>

```bash
$ curl -n -X PUT https://api.twixly.com/v1/buckets/[bucket_id]/items/[item_id] \
  -d {
    "data": {
      "type": "items",
      "attributes": {
        "title": "My first blog post changed",
        "excerpt": "A short excerpt",
        "body": "My body text"
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "items",
    "id": "5888000250dd41f022229fdc",
    "attributes": {
      "title": "My first blog post changed",
      "excerpt": "A short excerpt",
      "body": "My body text"
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "item_type": {
        "data": {
          "type": "item-types",
          "id": "post"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "created_at": "2017-01-25T01:31:46.755Z",
      "modified_at": "2017-01-25T01:31:46.755Z"
    }
  }
}
```

<h2 id="item-type-delete">Delete Item</h2>

Delete an item

```js
DELETE /item/<item_id>
```

<strong>Curl Example</strong>

```bash
$ curl -n -X DELETE https://api.twixly.com/v1/buckets/[bucket_id]/items/[item_id] \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "data": {
    "type": "items",
    "id": "5888000250dd41f022229fdc",
    "attributes": {
      "title": "My first blog post changed",
      "excerpt": "A short excerpt",
      "body": "My body text"
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "my-bucket"
        }
      },
      "item_type": {
        "data": {
          "type": "item-types",
          "id": "post"
        }
      },
      "status": "published",
      "position": 0,
      "parent_id": null,
      "created_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "modified_by": {
        "data": {
          "type": "users",
          "id": "587d6426eeaf110800880087"
        }
      },
      "created_at": "2017-01-25T01:31:46.755Z",
      "modified_at": "2017-01-25T01:31:46.755Z"
    }
  }
}
```