---
title: Content Model
---

Twixlys Content Model is based on a pair of endpoints that are very powerful in what they can do. They are **Bucket**, **Item Type**, **Item** and **Media**.

<div class="h2" style="margin-top: 25px;">Bucket</div>

Think of a **bucket** as a complete Content Model for a project. It could be a web site, mobile app, etc.

<div class="h2" style="margin-top: 25px;">**Item Type**</div>

Each **Item Type** contains one or more [fields](/field-types.html). [Fields](/field-types.html) may be of different types (such as string, integer, boolean, etc.). Think of an **Item Type** like data in a database. Each **Item Type** can be thought of as a table in a database, where the [fields](/field-types.html) of the **Item Type** represent the columns of the table and each individual **Item** represents a row in the table. An **Item Type** defines what data can be added to an **Item**.

<div class="h2" style="margin-top: 25px;">Item</div>

Every **Item** has a relationship to an **Item Type**. You can think of an **Item** as a blog post, product, article, etc. Or a row in a database table.

<div class="h2" style="margin-top: 25px;">Media</div>

Think of **Media** as a media library. You can upload binary data such as images, videos, documents.