---
title: Extension API Reference
---

<h2 class="h2" id="table-of-contents" style="margin-top: 25px;">`extension.itemType`</h2>

Gives you access to the current item type. The item type object is
documented in our [api documentation](http://docs.twixly.io/item-types.html).

## `extension.field`

This API gives you access to the value and metadata of the field the extension
is attached to.

| Name                                      | Arguments                         | Returns                                                    | Description                                  |
| ----------------------------------------- | --------------------------------- | ---------------------------------------------------------- | -------------------------------------------- |
| `extension.field.getValue()`              | none                              | Mixed value depending of [field type](/field-types.html)   | Gets the current value of the field          |
| `extension.field.setValue(value)`         | value: string                     | Promise:<void> Field                                       | Sets the value for the field                 |
| `extension.field.setInvalid(Boolean)`     | boolean: true/false               | undefined                                                  | Title for this extension                     |
| `extension.field.onChange(cb)`            | function                          | Field object                                               | URI to the extension                         |
| `extension.field.onValidate(cb)`          | function                          | Field object                                               | Extension type. `"field"` or `"view"`        |
| `extension.field.validate()`              | none                              |                                                            | Makes a validation of the field              |
| `extension.field.schema`                  | object                            |                                                            | The JSON Schema for this field               |

### `field.getValue(): mixed`

Gets the current value of the field.

### `field.setValue(value): Promise<void>`

Sets the value for the field. The promise is resolved when the change
has been acknowledged. The type of the value must match the expected field type.
For example, if the extension is attached to a "String" field you must pass a
string.

### `field.validate(): undefined`

Validates the field.

### `field.setInvalid(Boolean): undefined`

Communicates to the twixly web application if the field is in a valid state
or not. This impacts the styling applied to the field container.

### `field.onChange(cb): function`

Calls the callback every time the value of the field is changed by an external
event (e.g. when multiple editors are working on the same item) or when
`setValue()` is called.

### `field.onValidate(cb): function`

Calls the callback immediately with the current validation errors and whenever
the field is re-validated. The callback receives an array of error objects. An
empty array indicates no errors.

The errors are updated when the app validates an item. This happens when
loading an item or when the user tries to publish it. It also happens when
`validate()` is called.

### `field.id: string`

The ID of a field is defined in an item's item type.

## `extension.item`

This object allows you to read and update the value of any field of the current
item and to get the item's metadata.

### `item.fields[id]: Field`

In addition to [`extension.field`](#extensionfield), a extension can also
control the values of all other fields in the current item. Fields are
referenced by their ID.

The `Field` API methods provide a similar interface to `extension.field`.

- `field.id: string`
- `field.getValue(): mixed`
- `field.setValue(value): Promise<void>`
- `field.onChange(cb): function`

#### Example

If the item has a "title" field, you can transform it to upper case with:

```javascript
var titleField = extension.item.fields.title
var oldTitle = titleField.getValue();
titleField.setValue(oldTitle.toUpperCase());
```

## `extension.bucket`

The `bucket` object exposes methods that allow the extension to read and
manipulate a wide range of objects in the bucket.

### Item Types
### Items
### Media

Allows operating on the current bucket's item types, items and media.

- `bucket.get([item-type, item, media], options): Promise<void>`
- `bucket.get([item-type, item, media]/[id], options): Promise<void>`
- `bucket.post([item-type, item, media], data): Promise<void>`
- `bucket.put([item-type, item, media], data): Promise<void>`
- `bucket.delete(id): Promise<void>`

## `extension.window`

The window object provides methods to update the size of the iframe the
extension is contained within. This prevents scrollbars inside the extension.

To prevent a flickering scrollbar during height updates, it is recommended to
set the extension's `body` to `overflow: hidden;`.

### `window.updateHeight()`

Calculates the body's `scrollHeight` and sets the containers height to this
value.

### `window.updateHeight(height)`

Sets the iframe height to the given value in pixels. `height` must be an
integer.

## `extension.dialogs`

This object provides methods for opening UI dialogs:

### `dialogs.selectSingleitem(options)`

Opens a dialog for selecting a single item. It returns a promise resolved with
the selected entity or `null` if a user closes the dialog without selecting
anything.

`options` is an optional object configuring the dialog.The available `options`
are:

```javascript
// display a dialog for selecting a single item
dialogs.selectSingleitem().then((selecteditem) => {})

// select a single item of "blogpost" item type
dialogs.selectSingleitem({
  itemTypes: ['blogpost']
}).then((selecteditem) => {})
```

### dialogs.selectMultipleitems(options)

Works similarly to `selectSingleitem`, but allows to select multiple items
and the returned promise is resolved with an array of selected items.

- `min` and `max` - numeric values specifying an inclusive range in which the
number of selected entities must be contained

```javascript
// display a dialog for selecting multiple items
dialogs.selectMultipleitems().then((arrayOfSelecteditems) => {})

// select between 1 and 3 (inclusive) items
dialogs.selectMultipleitems({min: 1, max: 3})
.then((arrayOfSelecteditems) => {})
```

### `dialogs.selectSingleMedia(options)`

Counterpart of `selectSingleitem` for media. A `itemTypes` option is not
available.

### `dialogs.selectMultipleMedia(options)`

Counterpart of `selectMultipleitems` for media. A `itemTypes` option is
not available.




































