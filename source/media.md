---
title: Media
---

Learn how to create, read, update and delete media files.

<h2 id="create-media">Create Media</h2>

Create a new media file

```js
POST /media
```
<!--
<h3 id="create-media-required-parameters">Required Parameters</h3>
-->
<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.type`             | string | Resource type | `"media"`   |
| `data.attributes.file.url` | string | URI to the file to upload   | `http://example.com/image.jpg` |
| `data.attributes.file.content_type` | string | Mime type    | `image/jpeg` |

<strong>Curl Example</strong>

```bash
$ curl -n -X POST https://api.twixly.com/v1/buckets/[bucket_id]/media \
  -d {
    "data": {
      "type": "media",
      "attributes": {
        "file": {
          "url": "http://example.com/image.jpg",
          "content_type": "image/jpeg"
        }
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "type": "media",
  "id": "587c06e509509df916ab104e",
    "attributes": {
      "title": "image.jpg",
      "description": "",
      "file": {
        "url": "https://s3.amazonaws.com/twixly/58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-photo-5.jpg",
        "name": "368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "pathname": "58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "content_type": "image/jpeg",
        "size": 76428
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "5876db3831ad7e0a60b66ffd"
      }
    },
    "status": "published",
    "created_at": "2017-01-15T23:33:57.868Z",
    "modified_at": "2017-01-15T23:33:57.868Z"
  }
}
```

<h2 id="item-type-get">Get Media</h2>

Get a media items

```js
GET /media/<media_id>
```

<strong>Curl Example</strong>

```js
$ curl -n https://api.twixly.com/v1/buckets/[bucket_id]/media/[media_id] \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "type": "media",
  "id": "587c06e509509df916ab104e",
    "attributes": {
      "title": "image.jpg",
      "description": "",
      "file": {
        "url": "https://s3.amazonaws.com/twixly/58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-photo-5.jpg",
        "name": "368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "pathname": "58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "content_type": "image/jpeg",
        "size": 76428
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "5876db3831ad7e0a60b66ffd"
      }
    },
    "status": "published",
    "created_at": "2017-01-15T23:33:57.868Z",
    "modified_at": "2017-01-15T23:33:57.868Z"
  }
}
```

<h2 id="update-media">Update Media</h2>

Update a media item

```js
PUT /media/<media_id>
```
<strong>Required Parameters</strong>

| Name                  | Type   |      Description           |  Example        |
|-----------------------|--------|----------------------------|-----------------|
| `data.type`             | string | Resource type | `"media"`   |
| `data.attributes.file.url` | string | URI to the file to upload   | `http://example.com/image.jpg` |
| `data.attributes.file.content_type` | string | Mime type    | `image/jpeg` |

<strong>Curl Example</strong>

```bash
$ curl -n -X PUT https://api.twixly.com/v1/buckets/[bucket_id]/media \
  -d {
    "data": {
      "type": "media",
      "attributes": {
        "file": {
          "url": "http://example.com/image.jpg",
          "content_type": "image/jpeg"
        }
      }
    }
  } \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "type": "media",
  "id": "587c06e509509df916ab104e",
    "attributes": {
      "title": "image.jpg",
      "description": "",
      "file": {
        "url": "https://s3.amazonaws.com/twixly/58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-photo-5.jpg",
        "name": "368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "pathname": "58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "content_type": "image/jpeg",
        "size": 76428
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "5876db3831ad7e0a60b66ffd"
      }
    },
    "status": "published",
    "created_at": "2017-01-15T23:33:57.868Z",
    "modified_at": "2017-01-15T23:33:57.868Z"
  }
}
```


<h2 id="media-delete">Delete Media</h2>

Delete a media item

```js
DELETE /media/<media_id>
```

<strong>Curl Example</strong>

```bash
$ curl -n -X DELETE https://api.twixly.com/v1/buckets/[bucket_id]/media/[media_id] \
  -H "Content-Type: application/json" \
  -H "Accept: application/json" \
  -H "Authorization: Bearer [your_access_token]"
```

<strong>Response Example</strong>

```js
{
  "type": "media",
  "id": "587c06e509509df916ab104e",
    "attributes": {
      "title": "image.jpg",
      "description": "",
      "file": {
        "url": "https://s3.amazonaws.com/twixly/58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-photo-5.jpg",
        "name": "368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "pathname": "58ce8db6bef5cdfa4bc09e26/368b5780-1298-11e7-9182-79e7364c6127-image.jpg",
        "content_type": "image/jpeg",
        "size": 76428
      }
    },
    "meta": {
      "bucket": {
        "data": {
          "type": "buckets",
          "id": "5876db3831ad7e0a60b66ffd"
      }
    },
    "status": "published",
    "created_at": "2017-01-15T23:33:57.868Z",
    "modified_at": "2017-01-15T23:33:57.868Z"
  }
}
```